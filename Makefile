Flags=-Wall -Wextra
all:
	$(CC) $(Flags) -o arith main.c
debug:
	$(CC) $(Flags) -g -o arith_debug main.c
clean:
	rm arith arith_debug
install:
	cp arith /usr/local/bin
uninstall:
	rm /usr/local/bin/arith
