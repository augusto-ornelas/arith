# Arith

A simple program to quickly practice arithmetic calculations.

## Build

Simply run `make`, this will generate the `arith` binary

```console
$ make
```

## Install

Simply run:

```console
$ git clone https://gitlab.com/augusto-ornelas/arith.git
$ cd arith
$ make
$ make install
```

If you have an error about permission when running `make install` just run `sudo make install`.

## Usage

After installing you will have `arith` available in your `PATH`. Now you can run `arith`.

```console
$ arith
```

