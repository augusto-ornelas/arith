#define _POSIX_C_SOURCE 200809L
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define MINUTE 60
#define HOUR 3600
#define MEM_SIZE 100

enum { ADD, SUB, MUL, DIV, MOD };
// Some operations are easier than others
// I will consider that add, sub, mod are easy
// and mul, div are harder
// therefore the dificulty should increase at different rates

// After 10 exercises with add, sub and mod the level should increase
// I will like to practice all the multiplication tables before moving on to the
// next level Division is a bit easiear than multiplication and I will like to
// increase the level after 20 exercises completed

// TODO: The level indicates the number of digits in the lhs and the maximum
// number of digits in the rhs.
// For example:
// level 1 has 0 <= lhs <= 9 && 0 <= rhs <= 9
// level 2 has 0 <= lhs <= 99 && 0 <= rhs <= 9

struct op_data {
	int add;
	int sub;
	int mul;
	int div;
	int mod;
};

struct operands {
	int lhs;
	int rhs;
};

// make sure that random numbers are only repeated after all numbers
// appear once before
void strict_rand(char memory[][MEM_SIZE], int max_left, int max_right,
				 struct operands *oprds)
{
	int i;
	int j;
	int l = rand() % max_left;
	int r = rand() % (max_right - 1) + 1;

	// limit the number of zeroes to 1 per operation
	if (l == 0) {
		for (i = 0; i < MEM_SIZE; i++) {
			memory[0][i] = 1;
			memory[i][0] = 1;
		}
	}

	// limit the number of ones (1) to 1 per operation
	if (l == 1 || r == 1) {
		for (i = 1; i < MEM_SIZE; i++) {
			memory[1][i] = 1;
			memory[i][1] = 1;
		}
	}

	// TODO: This should not apply for multiplication
	if (l == r) {
		for (i = 1; i < MEM_SIZE; i++) {
			memory[i][i] = 1;
		}
	}

	if (!memory[l][r]) {
		memory[l][r] = 1;
		oprds->lhs = l;
		oprds->rhs = r;
		return;
	}
	for (i = 1; i < MEM_SIZE; i++) {
		for (j = 1; j < MEM_SIZE; j++) {
			if (!memory[i][j]) {
				memory[i][j] = 1;
				oprds->lhs = i;
				oprds->rhs = j;
				return;
			}
		}
	}
	memset((void *)memory, 0, MEM_SIZE * MEM_SIZE);
	oprds->lhs = l;
	oprds->rhs = r;
	memory[l][r] = 1;
	return;
}

static char memory_add[MEM_SIZE][MEM_SIZE];
static char memory_sub[MEM_SIZE][MEM_SIZE];
static char memory_mul[MEM_SIZE][MEM_SIZE];
static char memory_div[MEM_SIZE][MEM_SIZE];
static char memory_mod[MEM_SIZE][MEM_SIZE];

void rand_operands(int op, struct operands *oprds, int max_rand)
{
	switch (op) {
	case ADD:
		strict_rand(memory_add, max_rand, max_rand, oprds);
		return;
	case SUB:
		strict_rand(memory_sub, max_rand, max_rand, oprds);
		return;
	case MUL:
		// I don't want to increase the level of the multiplication for now
		strict_rand(memory_mul, 10, 10, oprds);
		return;
	case DIV:
		// I want that the rhs is less than the lhs
		strict_rand(memory_div, max_rand, 10, oprds);
		return;
	case MOD:
		// I would prefer if the random function is not random and produces more
		// numbers in the middle of the range
		strict_rand(memory_mod, max_rand, max_rand, oprds);
		return;
	default:
		fprintf(stderr, "Panic: invalid operation %d\n", op);
		exit(1);
	}
}

char *opt_to_str(int op)
{
	switch (op) {
	case ADD:
		return "+";
	case SUB:
		return "-";
	case MUL:
		return "*";
	case DIV:
		return "/";
	case MOD:
		return "%";
	default:
		fprintf(stderr, "Panic: invalid operation %d\n", op);
		exit(1);
	}
}

int result(int lhs, int rhs, int op)
{
	switch (op) {
	case ADD:
		return lhs + rhs;
	case SUB:
		return lhs - rhs;
	case MUL:
		return lhs * rhs;
	case DIV:
		return lhs / rhs;
	case MOD:
		return lhs % rhs;
	default:
		fprintf(stderr, "Panic: invalid operation %d\n", op);
		exit(1);
	}
}

int rand_rhs(int op)
{
	if (op == DIV || op == MOD) {
		return rand() % 9 + 1; // a random number from 1 to 9
	}
	return rand() % 10; // a random number from 0 to 9
}

void human_readable_duration(char *self, long duration)
{
	if (duration < MINUTE) {
		sprintf(self, "%ld seconds", duration);
	} else if (duration < HOUR) {
		sprintf(self, "%.2f minutes", duration / 60.0);
	} else {
		sprintf(self, "%.2f hours", duration / 3600.0);
	}
}

// it will keep track of operations that have been completed until now
// each index represents an operation, this serves to make the operations
// not repeat before all different operations have been presented
static char memory_op[5];

int strict_rand_op(int m_len)
{
	int i;
	int r = rand() % m_len;
	if (!memory_op[r]) {
		memory_op[r] = 1;
		return r;
	}
	for (i = 0; i < m_len; i++) {
		if (!memory_op[i]) {
			memory_op[i] = 1;
			return i;
		}
	}
	memset((void *)memory_op, 0, m_len);
	memory_op[r] = 1;
	return r;
}

static char *welcome_message =
	"\033[1;32mWelcome! Let's practice "
	"arithmetics\033[22;39m (press control+d to quit)";

void increase_completed_op(struct op_data *completed, int op)
{
	switch (op) {
	case ADD:
		if (completed->add < 10)
			completed->add++;
		return;
	case SUB:
		if (completed->sub < 10)
			completed->sub++;
		return;
	case MUL:
		if (completed->mul < 10)
			completed->mul++;
		return;
	case DIV:
		if (completed->div < 10)
			completed->div++;
		return;
	case MOD:
		if (completed->mod < 10)
			completed->mod++;
		return;
	default:
		fprintf(stderr, "Panic: invalid operation %d\n", op);
		exit(1);
	}
}

void increase_level(int *level, struct op_data *completed)
{
	if (completed->add >= level[ADD]) {
		level[ADD] *= 10;
		return;
	}
	if (completed->sub >= level[SUB]) {
		level[SUB] *= 10;
		return;
	}
	if (completed->div >= level[DIV]) {
		level[DIV] *= 10;
		return;
	}
	if (completed->mod >= level[MOD]) {
		level[MOD] *= 10;
	}
}

int main()
{
	srand(time(NULL));

	// keep track of the level in each operation
	int level[5];
	level[ADD] = 10;
	level[SUB] = 10;
	level[MUL] = 10;
	level[DIV] = 10;
	level[MOD] = 10;

	struct op_data completed_per_op;
	memset((void *)(&completed_per_op), 0, sizeof(completed_per_op));

	struct operands oprands;
	int op = strict_rand_op(5);
	rand_operands(op, &oprands, 10);
	int lhs = oprands.lhs;
	int rhs = oprands.rhs;
	int user_input;
	unsigned int completed = 0;
	time_t start = time(NULL);
	char *input_line = NULL;
	size_t input_len = 0;
	char *endptr = NULL;
	char hr_duration[60];
	time_t end;

	printf("%s\n\n", welcome_message);
	printf("\t\033[1m%d \033[35m%s\033[39m %d = \033[22m", lhs, opt_to_str(op),
		   rhs);

	while (getline(&input_line, &input_len, stdin) != -1) {
		printf("\033[2A\033[0J");
		if (*input_line == 'q')
			break;
		user_input = strtol(input_line, &endptr, 10);
		if (user_input == result(lhs, rhs, op)) {
			completed++;
			increase_completed_op(&completed_per_op, op);
			increase_level(level, &completed_per_op);
			printf("\t\033[32m%d correct! 😀 One more!\033[39m\n", completed);
			op = strict_rand_op(5);
			rand_operands(op, &oprands, level[op]);
			lhs = oprands.lhs;
			rhs = oprands.rhs;
		} else {
			printf("\t\033[31mThat's not right! Try again!\033[39m\n");
		}
		printf("\t\033[1m%d \033[35m%s\033[39m %d =\033[22m ", lhs,
			   opt_to_str(op), rhs);
	}
	free(input_line);

	end = time(NULL);
	human_readable_duration(hr_duration, end - start);
	printf("\n🥳 Nice! You practiced for %s and got %u exercises "
		   "correct!\n",
		   hr_duration, completed);

	return 0;
}
